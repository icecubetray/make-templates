# Makefile templates
This repository contains language-specific templates for GNU Make, each under their own branches.

## Currently available
- [C](../../tree/language/c)
